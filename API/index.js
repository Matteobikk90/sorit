const express = require('express');
const cors = require('cors');
const port = process.env.PORT || 3000;

const app = express();

app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.post('/send', (req, res) => {
  const { email, name, message, phone, profession } = req.body;
  sendMail(email, name, message, phone, profession, (err, data) => {
    if (err) {
      res.status(500).json({ message: 'Internal error' });
    } else {
      res.json({ message: 'Email sent' });
    }
  });
  res.json('Message received');
});

app.listen(port, () => {
  console.log(`app running on port ${port}`);
});
