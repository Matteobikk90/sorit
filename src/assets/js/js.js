const header = document.querySelector('header');
const burgerMenu = document.querySelector('#burger_menu');
const aside = document.querySelector('aside');
const asideLinks = document.querySelectorAll('aside a');
const scrollTop = document.querySelector('#scrollTop');
let idleTime = 0;
let x = 0;
const faders = document.querySelectorAll('.fade_in');
const sliders_in = document.querySelectorAll('.slide_in');
const backgroundHomepage = document.querySelector('.so_section1 .background');
const progettazionesSelected = document.querySelectorAll('.listed_projects');
const progettazioneCarousel = document.querySelector('.carousel.slide');
const progettazioneCarouselInner = document.querySelector('.carousel-inner');
const progettazioneCarouselControls = document.querySelectorAll('.controls');
const contactForm = document.querySelector('#contactForm');
const contactFormChecked = document.querySelector('#agreeContact');
const clickedPartners = document.querySelectorAll('.so_section3 a');
const partnerSection = document.querySelector('.so_details_section1');
const returns = document.querySelectorAll('.return');

const homeSlides = [
  'https://agsoritilluminazione.com/src/assets/images/IMMAGINE-SITO-2.jpg',
  'https://agsoritilluminazione.com/src/assets/images/IMMAGINE-SITO.jpg',
];

const villas = [
  'https://agsoritilluminazione.com/src/assets/images/villa/1.jpg',
  'https://agsoritilluminazione.com/src/assets/images/villa/2.jpg',
  'https://agsoritilluminazione.com/src/assets/images/villa/3.jpg',
  'https://agsoritilluminazione.com/src/assets/images/villa/4.jpg',
  'https://agsoritilluminazione.com/src/assets/images/villa/5.jpg',
];

const fortezza = [
  'https://agsoritilluminazione.com/src/assets/images/fortezza/1.jpg',
  'https://agsoritilluminazione.com/src/assets/images/fortezza/2.jpg',
  'https://agsoritilluminazione.com/src/assets/images/fortezza/3.jpg',
  'https://agsoritilluminazione.com/src/assets/images/fortezza/4.jpg',
];

const galleria = [
  'https://agsoritilluminazione.com/src/assets/images/galleria/1.jpg',
  'https://agsoritilluminazione.com/src/assets/images/galleria/2.jpg',
  'https://agsoritilluminazione.com/src/assets/images/galleria/3.jpg',
];

const loft = [
  'https://agsoritilluminazione.com/src/assets/images/loft/1.jpg',
  'https://agsoritilluminazione.com/src/assets/images/loft/2.jpg',
  'https://agsoritilluminazione.com/src/assets/images/loft/3.jpg',
  'https://agsoritilluminazione.com/src/assets/images/loft/4.jpg',
  'https://agsoritilluminazione.com/src/assets/images/loft/5.jpg',
];

const teatro = [
  'https://agsoritilluminazione.com/src/assets/images/teatro/1.jpg',
  'https://agsoritilluminazione.com/src/assets/images/teatro/2.jpg',
  'https://agsoritilluminazione.com/src/assets/images/teatro/3.jpg',
  'https://agsoritilluminazione.com/src/assets/images/teatro/5.jpg',
  'https://agsoritilluminazione.com/src/assets/images/teatro/7.jpg',
  'https://agsoritilluminazione.com/src/assets/images/teatro/8.jpg',
  'https://agsoritilluminazione.com/src/assets/images/teatro/9.jpg',
  'https://agsoritilluminazione.com/src/assets/images/teatro/11.jpg',
  'https://agsoritilluminazione.com/src/assets/images/teatro/12.jpg',
  'https://agsoritilluminazione.com/src/assets/images/teatro/14.jpg',
  'https://agsoritilluminazione.com/src/assets/images/teatro/17.jpg',
  'https://agsoritilluminazione.com/src/assets/images/teatro/26.jpg',
  'https://agsoritilluminazione.com/src/assets/images/teatro/27.jpg',
  'https://agsoritilluminazione.com/src/assets/images/teatro/29.jpg',
  'https://agsoritilluminazione.com/src/assets/images/teatro/30.jpg',
  'https://agsoritilluminazione.com/src/assets/images/teatro/37.jpg',
];

const priv2 = [
  'https://agsoritilluminazione.com/src/assets/images/residenza-privata-2/1.jpg',
  'https://agsoritilluminazione.com/src/assets/images/residenza-privata-2/2.jpg',
  'https://agsoritilluminazione.com/src/assets/images/residenza-privata-2/4.jpg',
  'https://agsoritilluminazione.com/src/assets/images/residenza-privata-2/7.jpg',
  'https://agsoritilluminazione.com/src/assets/images/residenza-privata-2/8.jpg',
  'https://agsoritilluminazione.com/src/assets/images/residenza-privata-2/9.jpg',
  'https://agsoritilluminazione.com/src/assets/images/residenza-privata-2/13.jpg',
  'https://agsoritilluminazione.com/src/assets/images/residenza-privata-2/14.jpg',
  'https://agsoritilluminazione.com/src/assets/images/residenza-privata-2/16.jpg',
  'https://agsoritilluminazione.com/src/assets/images/residenza-privata-2/17.jpg',
  'https://agsoritilluminazione.com/src/assets/images/residenza-privata-2/18.jpg',
];

const priv3 = [
  'https://agsoritilluminazione.com/src/assets/images/residenza-privata-3/1.jpg',
  'https://agsoritilluminazione.com/src/assets/images/residenza-privata-3/2.jpg',
  'https://agsoritilluminazione.com/src/assets/images/residenza-privata-3/6.jpg',
  'https://agsoritilluminazione.com/src/assets/images/residenza-privata-3/8.jpg',
  'https://agsoritilluminazione.com/src/assets/images/residenza-privata-3/10.jpg',
  'https://agsoritilluminazione.com/src/assets/images/residenza-privata-3/11.jpg',
  'https://agsoritilluminazione.com/src/assets/images/residenza-privata-3/12.jpg',
  'https://agsoritilluminazione.com/src/assets/images/residenza-privata-3/13.jpg',
  'https://agsoritilluminazione.com/src/assets/images/residenza-privata-3/14.jpg',
];

const baita = [
  'https://agsoritilluminazione.com/src/assets/images/baita/1.jpg',
  'https://agsoritilluminazione.com/src/assets/images/baita/2.jpg',
  'https://agsoritilluminazione.com/src/assets/images/baita/3.jpg',
  'https://agsoritilluminazione.com/src/assets/images/baita/4.jpg',
  'https://agsoritilluminazione.com/src/assets/images/baita/5.jpg',
  'https://agsoritilluminazione.com/src/assets/images/baita/6.jpg',
  'https://agsoritilluminazione.com/src/assets/images/baita/7.jpg',
  'https://agsoritilluminazione.com/src/assets/images/baita/8.jpg',
];

const dialux = [
  'https://agsoritilluminazione.com/src/assets/images/posizionamento-e-verifica-Dialux/1.jpg',
  'https://agsoritilluminazione.com/src/assets/images/posizionamento-e-verifica-Dialux/2.jpg',
  'https://agsoritilluminazione.com/src/assets/images/posizionamento-e-verifica-Dialux/3.jpg',
  'https://agsoritilluminazione.com/src/assets/images/posizionamento-e-verifica-Dialux/4.jpg',
  'https://agsoritilluminazione.com/src/assets/images/posizionamento-e-verifica-Dialux/5.jpg',
  'https://agsoritilluminazione.com/src/assets/images/posizionamento-e-verifica-Dialux/6.jpg',
  'https://agsoritilluminazione.com/src/assets/images/posizionamento-e-verifica-Dialux/7.jpg',
  'https://agsoritilluminazione.com/src/assets/images/posizionamento-e-verifica-Dialux/8.jpg',
];

const ilo = [
  'https://agsoritilluminazione.com/src/assets/images/progetto-sede-ILO-Torino/3.jpg',
  'https://agsoritilluminazione.com/src/assets/images/progetto-sede-ILO-Torino/4.jpg',
  'https://agsoritilluminazione.com/src/assets/images/progetto-sede-ILO-Torino/5.jpg',
  'https://agsoritilluminazione.com/src/assets/images/progetto-sede-ILO-Torino/6.jpg',
  'https://agsoritilluminazione.com/src/assets/images/progetto-sede-ILO-Torino/7.jpg',
];

const appearOnScroll = new IntersectionObserver(function (
  entries,
  appearOnScroll
) {
  entries.forEach((entry) => {
    if (!entry.isIntersecting) {
      return;
    } else {
      entry.target.classList.add('appear');
      appearOnScroll.unobserve(entry.target);
    }
  });
});

faders.forEach((fader) => {
  appearOnScroll.observe(fader);
});

sliders_in.forEach((slider_in) => {
  appearOnScroll.observe(slider_in);
});

const handleBurgerMenu = (e) => {
  e.currentTarget.classList.toggle('active');
  aside.classList.toggle('active');
};

const hideAside = () => {
  aside.classList.remove('active');
  burgerMenu.classList.remove('active');
};

const handleScroll = () => {
  header.classList.add('scroll');
};

const handleRemoveScroll = () => {
  header.classList.remove('scroll');
};

const checkHeader = () => {
  let scrollPosition = Math.round(window.scrollY);
  if (scrollPosition > 100) {
    handleScroll();
  } else {
    handleRemoveScroll();
  }
};

const toggleScrollTopBtn = () => {
  if (
    document.body.scrollTop > 400 ||
    document.documentElement.scrollTop > 400
  ) {
    scrollTop.classList.add('active');
  } else {
    scrollTop.classList.remove('active');
  }
};

const resetIdle = () => {
  idleTime = 0;
};

const timerIncrement = () => {
  idleTime = idleTime + 1;
  if (idleTime > 1) {
    scrollTop.style.opacity = 0;
  } else {
    scrollTop.style.opacity = 0.7;
  }
};

const scrollToTop = () => {
  // document.body.scrollTop = 0;
  // document.documentElement.scrollTop = 0;
  $('html').animate({ scrollTop: 0 }, 'slow');
};

const handleHomepageSlides = () => {
  if (x < homeSlides.length) {
    x = x + 1;
  } else {
    x = 1;
  }

  if (backgroundHomepage) {
    backgroundHomepage.style.backgroundImage = 'url(' + homeSlides[x - 1] + ')';
  }
};

const handleClickedPartner = (e) => {
  sessionStorage.setItem('partner', e.currentTarget.dataset.partner);
};

// data.js
const datas = [
  {
    id: 'braga',
    // pic:
    //   'https://agsoritilluminazione.com/src/assets/images/aziende/braga.jpg',
    logo:
      'https://agsoritilluminazione.com/src/assets/logo/aziende/braga-logo.png',
    name: 'F.lli Braga',
    nomeCataloghi: ['Catalogo_Braga_2017.pdf', 'Catalogo_Braga_2019.pdf'],
    cataloghi: [
      'https://agsoritilluminazione.com/src/assets/cataloghi/Catalogo-Braga-2017.pdf',
      'https://agsoritilluminazione.com/src/assets/cataloghi/Catalogo-Braga-2019.pdf',
    ],
    logoCataloghi: [
      'https://agsoritilluminazione.com/src/assets/images/aziende/braga-2017.jpg',
      'https://agsoritilluminazione.com/src/assets/images/aziende/braga-2019.jpg',
    ],
    info:
      'Braga Illuminazione è protagonista del mercato da 30 anni con una produzione artigianale 100% made in Italy.  Questa esperienza aquisita negli anni ed un know-how altamente specializzato garantiscono la professionalità e la competenza di Braga Illuminazione nella creazione di soluzioni qualitativamente superiori per l’illuminazione di ogni ambiente, dal più classico al moderno. In risposta alle tendenze del settore e per soddisfare le esigenze di risparmio energetico senza rinunciare alla resa estetica, Braga Illuminazione propone un ampio assortimento di lampade a led altamente efficienti e lampadari a led declinati in svariati modelli: incasso, a parete, a soffitto, a sospensione, da tavolo e da terra.',
  },
  {
    id: 'cattaneo',
    // pic:
    //   'https://agsoritilluminazione.com/src/assets/images/aziende/cattaneo.jpg',
    logo:
      'https://agsoritilluminazione.com/src/assets/logo/aziende/cattaneo-logo.png',
    name: 'Cattaneo',
    nomeCataloghi: [
      'Catalogo_cattaneo_Today.pdf',
      'Catalogo_cattaneo_Tomorrow.pdf',
    ],
    cataloghi: [
      'https://agsoritilluminazione.com/src/assets/cataloghi/Catalogo-Cattaneo-Today.pdf',
      'https://agsoritilluminazione.com/src/assets/cataloghi/Catalogo-Cattaneo-Tomorrow.pdf',
    ],
    logoCataloghi: [
      'https://agsoritilluminazione.com/src/assets/images/aziende/today-2020-cattaneo.jpg',
      'https://agsoritilluminazione.com/src/assets/images/aziende/tomorrow-2020-cattaneo.jpg',
    ],
    info:
      'Da oltre quarant’anni, Cattaneo Illuminazione propone creazioni che scaturiscono dalla fantasia e dalla manualità di qualificati artigiani italiani. La missione è quella di offrire alla clientela solo prodotti di alta qualità, che esprimano al meglio il valore estetico e tecnico caratterizzata dalla accurata ricerca dei materiali. L’entusiasmo di Cattaneo illuminazione vuole essere: “La scintilla di creatività che dall’Italia illumina il mondo”.',
  },
  {
    id: 'athena',
    // pic:
    //   'https://agsoritilluminazione.com/src/assets/images/aziende/athena.bmp',
    logo:
      'https://agsoritilluminazione.com/src/assets/logo/aziende/athena-logo.jpg',
    name: 'Athena',
    nomeCataloghi: ['Athena-Catalogo-2019.pdf'],
    cataloghi: [
      'https://agsoritilluminazione.com/src/assets/cataloghi/Athena-Catalogo-2019.pdf',
    ],
    logoCataloghi: [
      'https://agsoritilluminazione.com/src/assets/images/aziende/athena-logo-cat.jpg',
    ],
    info: `Presente sul mercato dal 1972, Athena concentra nella propria sede tutte le fasi di lavorazione dei prodotti, dalla progettazione, alla costruzione e all’assemblaggio. La massima professionalità viene impiegata nella creazione e nella produzione degli articoli per garantire la più accurata qualità e sicurezza; ogni modello rispetta infatti le attuali normative vigenti ed è dotato di marcatura europea CE attestante la conformità. L’esperienza dei tecnici e designers, la ricerca di nuovi materiali, l’utilizzo delle nuove tecnologie di costruzione caratterizzano l’unicità delle collezioni. La produzione Athena spazia tra collezioni di lampade da terra, da tavolo e da parete, plafoniere, sospensioni, faretti da incasso e sistemi su cavo a 220V e 12V.Di primaria importanza la produzione di profili in alluminio corredati da luci led che l’azienda propone in diverse misure ma che possono anche essere richieste con lavorazioni personalizzate. Le sezioni Decorativo , Tecnico ed Incasso raggruppano le collezioni nate per soddisfare tutte le esigenze di illuminazione negli ambienti di lavoro e commerciali; non di meno offrono soluzioni innovative per i vari ambienti della casa, ma anche idee illuminotecniche uniche per esecuzioni speciali o contract.`,
  },
  {
    id: 'italia',
    // pic:
    //   'https://agsoritilluminazione.com/src/assets/images/aziende/italia.png',
    logo:
      'https://agsoritilluminazione.com/src/assets/logo/aziende/italia-logo.png',
    name: 'Studio Italia Design',
    nomeCataloghi: ['New-Finishes.pdf', 'STUDIO_ITALIA_DESIGN_2019_LOW.pdf'],
    cataloghi: [
      'https://agsoritilluminazione.com/src/assets/cataloghi/New-Finishes.pdf',
      'https://agsoritilluminazione.com/src/assets/cataloghi/STUDIO_ITALIA_DESIGN_2019_LOW.pdf',
    ],
    logoCataloghi: [
      'https://agsoritilluminazione.com/src/assets/images/aziende/nuove-finiture-2020-studio-italia-design.png',
      'https://agsoritilluminazione.com/src/assets/images/aziende/Catalogo-2020-Studio-Italia-Design.png',
    ],
    info: `Fondata nel 1950 a Venezia, Studio Italia Design percorre da sempre la strada maestra del puro Made in Italy: le sua lampade sono realizzate con i migliori e più pregiati materiali, i diffusori in vetro soffiato a bocca nelle fornaci di Murano si uniscono all'ottone, all'alluminio ed all'acciaio nelle loro diverse finiture. 
    Scelte vincenti che hanno permesso a Studio Italia Design di essere presente nel mondo in più di 90 paesi con partners competenti ed affidabili.`,
  },
  {
    id: 'kundalini',
    // pic:
    //   'https://agsoritilluminazione.com/src/assets/images/aziende/italia.png',
    logo: [
      'https://agsoritilluminazione.com/src/assets/logo/aziende/kundalini.jpg',
    ],
    name: 'Kundalini',
    nomeCataloghi: ['KUNDALINI_Catalogo-2019.pdf'],
    cataloghi: [
      'https://agsoritilluminazione.com/src/assets/cataloghi/KUNDALINI_Catalogo-2019.pdf',
    ],
    logoCataloghi: [
      'https://agsoritilluminazione.com/src/assets/images/aziende/kundalini-logo.jpg',
    ],
    info: `Kundalini è un’azienda caratterizzata da una forte identità e da uno stile cosmopolita, innovativo e sofisticato.
    È un’azienda che parla un linguaggio universale e al contempo estremamente riconoscibile.
    Il suo spirito anticonvenzionale unisce da sempre alla sottile poesia della forma la massima funzionalità e praticità d’uso plasmando i migliori materiali attraverso lavorazioni d’avanguardia.`,
  },
  {
    id: 'masiero',
    // pic:
    //   'https://agsoritilluminazione.com/src/assets/images/aziende/masiero.jpg',
    logo:
      'https://agsoritilluminazione.com/src/assets/logo/aziende/masiero-logo.jpg',
    name: 'Masiero',
    nomeCataloghi: [
      'Catalogo-Atelier-2020.pdf',
      'Catalogo-Dimore-2020.pdf',
      'Catalogo-Drylight-2020.pdf',
    ],
    cataloghi: [
      'https://agsoritilluminazione.com/src/assets/cataloghi/ATELIER-Catalogo-2020.pdf',
      'https://agsoritilluminazione.com/src/assets/cataloghi/DIMORE-Catalogo-2020.pdf',
      'https://agsoritilluminazione.com/src/assets/cataloghi/DRYLIGHT-Catalogo-2020.pdf',
    ],
    logoCataloghi: [
      'https://agsoritilluminazione.com/src/assets/images/aziende/catalogo-atelier-download.png',
      'https://agsoritilluminazione.com/src/assets/images/aziende/DIMORE_Catalogo_2020.jpg',
      'https://agsoritilluminazione.com/src/assets/images/aziende/DRYLIGHT_Catalogo_2019.jpg',
    ],
    info: `L'azienda, specializzata nell'illuminazione decorativa d'alta gamma, di Casale sul Sile, nel trevigiano. Da qui parte tutto. Anche la sua storia che, come nella tradizione del Made in Italy è una storia di imprenditoria familiare, iniziata nel 1981. Dimore e Atelier, sono i due cataloghi che raccolgono e sviluppano le produzioni correnti di Masiero, allargate al segno contemporaneo e dirette rispettivamente a soluzioni residenziali e al contract. Ciascun prodotto prende vita all'interno degli stabilimenti, dove la specializzata catena produttiva copre l'intero ciclo, del quale l'ufficio tecnico e la sala prototipi sono il punto focale. Dalla lavorazione dei singoli pezzi grezzi all'assemblaggio, passando dalla verniciatura, finitura e decorazione; dall'impianto elettrico al collaudo fino all'imballo e alla spedizione, il processo è mirato al controllo della qualità e ad accogliere le richieste speciali. Manifattura e materiali di pregio, capacità tecnica e sartoriale e posizionamento di mercato in fascia alta: sono i differenti aspetti che rendono unico il brand Masiero.`,
  },
  {
    id: 'lighting',
    // pic:
    //   'https://agsoritilluminazione.com/src/assets/images/aziende/cattaneo.jpg',
    logo:
      'https://agsoritilluminazione.com/src/assets/logo/aziende/lighting-logo.png',
    name: 'Bot Lighting',
    nomeCataloghi: [
      'Catalogo Sorgenti e Accessori giugno 2019',
      'Catalogo Apparecchi ottobre 2019',
    ],
    cataloghi: [
      'https://agsoritilluminazione.com/src/assets/cataloghi/CATALOGO-2019-APPARECCHI-BOTLIGHTING.pdf',
      'https://agsoritilluminazione.com/src/assets/cataloghi/CATALOGO-2019-SORGENTI-BOTLIGHTING.pdf',
    ],
    logoCataloghi: [
      'https://agsoritilluminazione.com/src/assets/images/aziende/cover-bot-1.png',
      'https://agsoritilluminazione.com/src/assets/images/aziende/segnalino_bot.png',
    ],
    info: `Bot Lighting è protagonista nel settore dell’illuminotecnica da 40 anni. Con i marchi SHOT e KAI, e la distribuzione dei prodotti AIRAM, offre sorgenti luminose e apparecchi per ogni ambito civile e industriale. Grazie a una rete commerciale capillare, forte di figure esperte e professionali, distribuisce i suoi prodotti in Italia e all’estero e garantisce un servizio rapido ed efficiente. L’esperienza sul campo e l’investimento costante nella ricerca e nello sviluppo di soluzioni innovative hanno permesso all’azienda di instaurare con i propri clienti e partners un rapporto di fiducia, oltre a stimolare lo sviluppo di nuovi mercati.`,
  },
  {
    id: 'lux',
    // pic:
    //   'https://agsoritilluminazione.com/src/assets/images/aziende/cattaneo.jpg',
    logo:
      'https://agsoritilluminazione.com/src/assets/logo/aziende/lux-logo.png',
    name: 'Ideal Lux',
    nomeCataloghi: ['Catalogo Ideal Lux 2020'],
    cataloghi: [
      'https://agsoritilluminazione.com/src/assets/cataloghi/Catalogo-Ideal-Lux-2020.pdf',
    ],
    logoCataloghi: [
      'https://agsoritilluminazione.com/src/assets/images/aziende/ideal-cat.jpg',
    ],
    info: `Fondata nel 1974, Ideal Lux nasce come piccola realtà guidata dall’intuizione di ideare un prodotto attuale e al contempo accessibile a un’ampia fascia di pubblico, obiettivo perseguito ancora oggi. La nostra ambizione è di racchiudere in un unico marchio la più ampia varietà di stili possibili, allineati con le tendenze del settore spaziando dal decorativo classico al moderno, dai prodotti per l’esterno a quelli tecnici. L’attenzione per le persone è centrale. Ci mettiamo in ascolto per cogliere al meglio le loro esigenze in modo da rispondere non solo a una funzionalità, ma soprattutto a uno stile di vita. Essere presenti in cinque continenti ci da oggi un respiro internazionale che non ha cambiato il nostro modo di essere e operare come impresa:
    il gusto per le cose ben fatte, l’attitudine alla trasparenza, l’abitudine alla semplicità.`,
  },
  {
    id: 'trio',
    // pic:
    //   'https://agsoritilluminazione.com/src/assets/images/aziende/cattaneo.jpg',
    logo:
      'https://agsoritilluminazione.com/src/assets/logo/aziende/lighting-logo.svg',
    name: 'Trio Lighting',
    nomeCataloghi: [
      'Trio-2020.pdf',
      'Rl-2020.pdf',
      'Outdoor-2020.pdf',
      'News-2020.pdf',
    ],
    cataloghi: [
      'https://agsoritilluminazione.com/src/assets/cataloghi/TRIO_Hauptkatalog_2020.pdf',
      'https://agsoritilluminazione.com/src/assets/cataloghi/Reality_Hauptkatalog_2020.pdf',
      'https://agsoritilluminazione.com/src/assets/cataloghi/TRIO_Outdoor_2020.pdf',
      'https://agsoritilluminazione.com/src/assets/cataloghi/TRIO_news_2020.pdf',
    ],
    logoCataloghi: [
      'https://agsoritilluminazione.com/src/assets/images/aziende/page_1.jpg',
      'https://agsoritilluminazione.com/src/assets/images/aziende/page_1-1.jpg',
      'https://agsoritilluminazione.com/src/assets/images/aziende/page_1-2.jpg',
      'https://agsoritilluminazione.com/src/assets/images/aziende/page_1-3.jpg',
    ],
    info: `TRIO è stata fondata nel 1991 ad Arnsberg, Sauerland. Oggi, a distanza di quasi 30 anni, la nostra azienda si colloca tra gli specialisti leader nel settore dell'illuminazione decorativa per la casa. Il Gruppo TRIO Lighting conta circa 260 dipendenti e attualmente opera in oltre 30 paesi, tra cui Joint Venture in Italia, Spagna e Scandinavia. Idee di prodotto innovative, un'elevata flessibilità e un modo di pensare e di agire costantemente orientato al cliente sono le pietre miliari del nostro successo. Una delle iniziative più recenti nella nostra storia di successo è la costruzione della nuova sede aziendale nel parco industriale Gut Nierhof a Voßwinkel. Su una superficie di circa 30.000 m² vengono costruiti un nuovo edificio amministrativo e un modernissimo centro logistico con una capacità di stoccaggio di 45.000 posti pallet.`,
  },
  {
    id: 'platek',
    // pic:
    //   'https://agsoritilluminazione.com/src/assets/images/aziende/cattaneo.jpg',
    logo: 'https://agsoritilluminazione.com/src/assets/logo/aziende/platek.png',
    name: 'Platek',
    nomeCataloghi: ['Platek-Company-Profile.pdf'],
    cataloghi: [
      'https://agsoritilluminazione.com/src/assets/cataloghi/Platek-Company-Profile.pdf',
    ],
    logoCataloghi: [
      'https://agsoritilluminazione.com/src/assets/images/aziende/platek-cat.png',
    ],
    info: `Platek è un'azienda internazionale proiettata all'innovazione nel settore dell'illuminazione architetturale e decorativa per esterni. La gamma di prodotti comprende sistemi integrati di illuminazione architetturale, composti da incassi a pavimento, applique, sospensioni, proiettori e paletti, tutti con gruppi ottici con tecnologia LED progettati internamente e realizzati in esclusiva per Platek. Il grande know-how del gruppo Donati, del quale Platek fa parte, nella fusione dei metalli e delle leghe di alluminio ha permesso all'azienda di progettare e realizzare corpi illuminanti sempre più compatti e performanti, sia sotto il profilo della potenza luminosa che dell'efficienza energetica.`,
  },
  {
    id: 'zero',
    // pic:
    //   'https://agsoritilluminazione.com/src/assets/images/aziende/cattaneo.jpg',
    logo:
      'https://agsoritilluminazione.com/src/assets/logo/aziende/zero-logo.png',
    name: 'Linea Zero',
    nomeCataloghi: [
      'OUTDOOR_CATALISTINO_LINEA_ZERO.pdf',
      'LITOS_BROCHURE_2019.pdf',
    ],
    cataloghi: [
      'https://agsoritilluminazione.com/src/assets/cataloghi/OUTDOOR_CATALISTINO_LINEA_ZERO.pdf',
      'https://agsoritilluminazione.com/src/assets/cataloghi/LITOS_BROCHURE_2019.pdf',
    ],
    logoCataloghi: [
      'https://agsoritilluminazione.com/src/assets/images/aziende/CAT4.png',
      'https://agsoritilluminazione.com/src/assets/images/aziende/CAT4.png',
    ],
    info: `Linea Zero, azienda che opera nel settore dell’illuminazione di interni, è stata fondata a Verona nel 1973 da Enea Ferrari. Grazie al Polilux™, esclusivo materiale con cui vengono realizzati i prodotti, l’azienda progetta e produce lampade di design contemporaneo e funzionale. Il Made in Italy continua ad essere, per Linea Zero, il più importante dei valori aziendali. Da oltre 40 anni Linea Zero è sinonimo di ricerca e crescita continua, sempre rivolta ad interpretare al meglio le esigenze di mercati in rapida evoluzione, mettendo a punto precise politiche e strategie commerciali, ma sempre con particolare attenzione alle esigenze del cliente. Un percorso imprenditoriale dinamico che traccia i confini di un nuovo futuro, espressione di design, smart lighting e innovazione anche grazie ad un team rinnovato e giovane.`,
  },
];

if (sessionStorage.partner) {
  const filteredPartner = datas.filter(
    (data) => data.id === sessionStorage.partner
  );

  const htmlPartner = `
    <img src="${filteredPartner[0].logo}" alt="Logo Azienda Partner - ${
    filteredPartner[0].name
  }" 
    />
    <p>${filteredPartner[0].info}</p>
    <p>Clicca sulla copertina per scaricare il catalogo</p>
    <div class="cataloghi">
      ${filteredPartner[0].cataloghi
        .map(
          (catalogo, i) =>
            `
              <a href="${catalogo}" download>
                <img
                  src="${filteredPartner[0].logoCataloghi[i]}"
                  alt="Logo Catalogo Azienda Partner ${i + 1} - ${
              filteredPartner[0].name
            }"
                  titolo="Scarica Catalogo ${
                    filteredPartner[0].nomeCataloghi[i]
                  }"
                />
                ${
                  filteredPartner[0].id === 'lighting'
                    ? `
                    <span>${filteredPartner[0].nomeCataloghi[i]}</span>
                  `
                    : ''
                }
              </a>
          `
        )
        .join('')}
    </div>
`;

  // ${
  //   filteredPartner[0].pic
  //     ? `
  //         <img src="${filteredPartner[0].pic}" alt="Immagine Azienda Partner - ${filteredPartner[0].name}" />
  //       `
  //     : ''
  // }

  if (partnerSection) {
    partnerSection.innerHTML += htmlPartner;
    partnerSection.classList.add('appear');
  }
}

const handleProgettazioneCarousel = (e) => {
  const clickedProg = e.currentTarget.getAttribute('href');
  const filteredClickedProg = clickedProg.replace('#', '');
  progettazioneCarousel.setAttribute('id', filteredClickedProg);
  progettazioneCarouselControls.forEach((progettazioneCarouselControl) => {
    progettazioneCarouselControl.setAttribute('href', clickedProg);
  });

  progettazioneCarouselInner.innerHTML = '';
  let htmlCarouselInner = '';
  switch (filteredClickedProg) {
    case 'villa':
      villas.forEach((prod, i) => {
        if (i === 0) {
          htmlCarouselInner = `
                                    <div class="carousel-item active">
                                      <img
                                        src="${prod}"
                                        alt=""
                                      />
                                    </div>
                                    `;
        } else {
          htmlCarouselInner = `
                                    <div class="carousel-item">
                                      <img
                                        src="${prod}"
                                        alt=""
                                      />
                                    </div>
                                  `;
        }

        progettazioneCarouselInner.innerHTML += htmlCarouselInner;
      });
      break;
    case 'fortezza':
      fortezza.forEach((prod, i) => {
        if (i === 0) {
          htmlCarouselInner = `
                                    <div class="carousel-item active">
                                      <img
                                        src="${prod}"
                                        alt=""
                                      />
                                    </div>
                                    `;
        } else {
          htmlCarouselInner = `
                                    <div class="carousel-item">
                                      <img
                                        src="${prod}"
                                        alt=""
                                      />
                                    </div>
                                  `;
        }

        progettazioneCarouselInner.innerHTML += htmlCarouselInner;
      });
      break;
    case 'galleria':
      galleria.forEach((prod, i) => {
        if (i === 0) {
          htmlCarouselInner = `
                                    <div class="carousel-item active">
                                      <img
                                        src="${prod}"
                                        alt=""
                                      />
                                    </div>
                                    `;
        } else {
          htmlCarouselInner = `
                                    <div class="carousel-item">
                                      <img
                                        src="${prod}"
                                        alt=""
                                      />
                                    </div>
                                  `;
        }

        progettazioneCarouselInner.innerHTML += htmlCarouselInner;
      });
      break;
    case 'loft':
      loft.forEach((prod, i) => {
        if (i === 0) {
          htmlCarouselInner = `
                                    <div class="carousel-item active">
                                      <img
                                        src="${prod}"
                                        alt=""
                                      />
                                    </div>
                                    `;
        } else {
          htmlCarouselInner = `
                                    <div class="carousel-item">
                                      <img
                                        src="${prod}"
                                        alt=""
                                      />
                                    </div>
                                  `;
        }

        progettazioneCarouselInner.innerHTML += htmlCarouselInner;
      });
      break;
    case 'teatro':
      teatro.forEach((prod, i) => {
        if (i === 0) {
          htmlCarouselInner = `
                                    <div class="carousel-item active" data-slide-number="${i}">
                                      <img
                                        src="${prod}"
                                        alt=""
                                      />
                                    </div>
                                    `;
        } else {
          htmlCarouselInner = `
                                    <div class="carousel-item" data-slide-number="${i}">
                                      <img
                                        src="${prod}"
                                        alt=""
                                      />
                                    </div>
                                  `;
        }

        progettazioneCarouselInner.innerHTML += htmlCarouselInner;
      });
      break;
    case 'priv2':
      priv2.forEach((prod, i) => {
        if (i === 0) {
          htmlCarouselInner = `
                                    <div class="carousel-item active" data-slide-number="${i}">
                                      <img
                                        src="${prod}"
                                        alt=""
                                      />
                                    </div>
                                    `;
        } else {
          htmlCarouselInner = `
                                    <div class="carousel-item" data-slide-number="${i}">
                                      <img
                                        src="${prod}"
                                        alt=""
                                      />
                                    </div>
                                  `;
        }

        progettazioneCarouselInner.innerHTML += htmlCarouselInner;
      });
      break;
    case 'priv3':
      priv3.forEach((prod, i) => {
        if (i === 0) {
          htmlCarouselInner = `
                                      <div class="carousel-item active" data-slide-number="${i}">
                                        <img
                                          src="${prod}"
                                          alt=""
                                        />
                                      </div>
                                      `;
        } else {
          htmlCarouselInner = `
                                      <div class="carousel-item" data-slide-number="${i}">
                                        <img
                                          src="${prod}"
                                          alt=""
                                        />
                                      </div>
                                    `;
        }

        progettazioneCarouselInner.innerHTML += htmlCarouselInner;
      });
      break;
    case 'dialux':
      dialux.forEach((prod, i) => {
        if (i === 0) {
          htmlCarouselInner = `
                                        <div class="carousel-item active" data-slide-number="${i}">
                                          <img
                                            src="${prod}"
                                            alt=""
                                          />
                                        </div>
                                        `;
        } else {
          htmlCarouselInner = `
                                        <div class="carousel-item" data-slide-number="${i}">
                                          <img
                                            src="${prod}"
                                            alt=""
                                          />
                                        </div>
                                      `;
        }

        progettazioneCarouselInner.innerHTML += htmlCarouselInner;
      });
      break;
    case 'ilo':
      ilo.forEach((prod, i) => {
        if (i === 0) {
          htmlCarouselInner = `
                                          <div class="carousel-item active" data-slide-number="${i}">
                                            <img
                                              src="${prod}"
                                              alt=""
                                            />
                                          </div>
                                          `;
        } else {
          htmlCarouselInner = `
                                          <div class="carousel-item" data-slide-number="${i}">
                                            <img
                                              src="${prod}"
                                              alt=""
                                            />
                                          </div>
                                        `;
        }

        progettazioneCarouselInner.innerHTML += htmlCarouselInner;
      });
      break;
    case 'baita':
      baita.forEach((prod, i) => {
        if (i === 0) {
          htmlCarouselInner = `
                                          <div class="carousel-item active" data-slide-number="${i}">
                                            <img
                                              src="${prod}"
                                              alt=""
                                            />
                                          </div>
                                          `;
        } else {
          htmlCarouselInner = `
                                          <div class="carousel-item" data-slide-number="${i}">
                                            <img
                                              src="${prod}"
                                              alt=""
                                            />
                                          </div>
                                        `;
        }

        progettazioneCarouselInner.innerHTML += htmlCarouselInner;
      });
      break;
    default:
      console.log('Not found');
  }
};

const handleReturnSessionStorage = () => {
  if (sessionStorage.partner) {
    sessionStorage.removeItem('partner');
  }
};

//Increment the idle time counter every 3 seconds.
const idleInterval = setInterval(timerIncrement, 3000);

const handleContactForm = (e) => {
  e.preventDefault();
  const name = document.getElementById('name').value;
  const email = document.getElementById('email').value;
  const phone = document.getElementById('phone').value;
  const profession = document.getElementById('profession').value;
  const message = document.getElementById('message').value;

  const data = {
    name,
    email,
    phone,
    profession,
    message,
  };

  if (contactFormChecked.checked) {
    var url = 'https://sorit-api.herokuapp.com/send';
    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
      body: JSON.stringify(data),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        alert('Messaggio inviato correttamente');
        contactForm.reset();
      })
      .catch((err) => {
        console.log(err);
        alert('Messaggio non inviato, riprova');
      });
  }
};

asideLinks.forEach((asideLink) => {
  asideLink.addEventListener('click', hideAside);
});
progettazionesSelected.forEach((progettazioneSelected) => {
  progettazioneSelected.addEventListener('click', handleProgettazioneCarousel);
});
returns.forEach((returnDelete) => {
  returnDelete.addEventListener('click', handleReturnSessionStorage);
});
burgerMenu.addEventListener('click', handleBurgerMenu);
scrollTop.addEventListener('click', scrollToTop);
scrollTop.addEventListener('keypress', scrollToTop);
window.addEventListener('scroll', () => {
  checkHeader(), toggleScrollTopBtn(), resetIdle();
});
//Zero the idle timer on mouse movement.
window.addEventListener('mousemove', resetIdle);
window.addEventListener('keypress', resetIdle);
//Zero the idle timer on touch events.
window.addEventListener('touchstart', resetIdle);
window.addEventListener('touchmove', resetIdle);

if (contactForm) {
  contactForm.addEventListener('submit', handleContactForm);
}
if (clickedPartners) {
  clickedPartners.forEach((clickedPartner) =>
    clickedPartner.addEventListener('click', handleClickedPartner)
  );
}

setInterval(handleHomepageSlides, 5000);

// This function will show the image in the lightbox
var zoomImg = function () {
  // Create evil image clone
  var clone = this.cloneNode();
  clone.classList.remove('zoomD');

  // Put evil clone into lightbox
  var lb = document.getElementById('lb-img');
  lb.innerHTML = '';
  lb.appendChild(clone);

  // Show lightbox
  lb = document.getElementById('lb-back');
  lb.classList.add('show');
};

window.addEventListener('load', function () {
  // Attach on click events to all .zoomD images
  var images = document.getElementsByClassName('zoomD');
  if (images.length > 0) {
    for (var img of images) {
      img.addEventListener('click', zoomImg);
    }
  }

  // Click event to hide the lightbox
  document.getElementById('lb-back').addEventListener('click', function () {
    this.classList.remove('show');
  });
});
